const scrapper = require('../src/service/scrapper.js');

const fbPagesUrl = process.argv.slice(2);
console.log('will fetch the pages : ', fbPagesUrl);

scrapper(fbPagesUrl)
  .then(() => {
    console.log('ok');
    process.exit(0);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
