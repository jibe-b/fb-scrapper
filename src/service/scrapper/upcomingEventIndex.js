const request = require('request-promise');

function fetch(fbPageId) {
  return request({
    uri: 'https://www.facebook.com/api/graphql/',
    method: 'POST',
    form: {
      av: '0',
      '__user=': '0',
      '__a': '1',
      '__dyn': '7xeUmFoHwUCJDzo9FE9XG8GAdyedJLFGUeEqzobpEpyEybGqK6otyEnwTyUmyQUC48G5WAxamjyUuKcUfFU9Egy-dxK4p94rzLxe488p8Gicx210wKx6HBxe6Ehwj8lwCAx2i10xadzVK5e9wg8tCwDwxzoGqfw-KE-2J16q2K8yodQ9wRyXyUpwxwnogUkBzUyey8eohyUjyo89eawGKUlzA7VEpxnyBxaUhwn8C1pxG1zxiQUy7EK2CaKE-3e22u8wxy8hG2m2-2l2UtxecgoBwlo9o9E4-5EgwNwCw',
      '__req': '6',
      '__be': '1',
      '__pc': 'PHASED%3ADEFAULT',
      'dpr': 1,
      '__rev': '1000701786',
      '__s': 'vhyoma:t30drp',
      'lsd': 'AVrHHBUX',
      'jazoest': '2648',
      'fb_api_caller_class': 'RelayModern',
      'fb_api_req_friendly_name':	'PageEventsTabUpcomingEventsCardRendererQuery',
      'variables': `{"pageID":"${fbPageId}","count":30}`,
      'doc_id': '2343886202319301'
    },
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  });
}

function parse(graphqlAnswer) {
  const json = JSON.parse(graphqlAnswer);
  const events = json.data.page.upcoming_events.edges;
  return events.map(edge => edge.node.id);
}

async function scrap(fbPageId) {
  const graphqlAnswer = await fetch(fbPageId);
  const eventIds = parse(graphqlAnswer);

  return eventIds;
}

module.exports = {
  scrap
};
